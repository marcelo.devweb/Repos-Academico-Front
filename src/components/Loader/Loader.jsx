import React from 'react';
import "./Loader.css";

const Loader = () => {
  return (
    <div className='loader flex flex-c'>
      <div class="c-loader"></div>
    </div>
  )
}

export default Loader