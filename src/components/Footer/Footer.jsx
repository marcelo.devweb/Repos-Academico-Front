import React from 'react';
import "./Footer.css"
  
const Footer = () => (
  <footer className="footer">
    <p>Development, Design & Template by Marcelo Moreira | Copyright© 2023</p>
  </footer>
);
  
export default Footer;